<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Home;

class HomeController extends Controller
{
    public function index(){
        $biodata = Home::all();
     //   $biodata = DB::$table('biodata')->get();
        return view('list', compact('biodata'));
    }
    
    public function tambah(){
        return view('tambah');
    }
    public function form(Request $request){

        $request->validate([
            'nama'=>'required',
            'alamat'=>'required',
            'jurusan'=>'required',
            'email'=>'required',
        ]);

        $nama = $request->input('nama');
        $alamat = $request->input('alamat');
        $jurusan = $request->input('jurusan');
        $email = $request->input('email');
    
    Home::create([
        'nama' => $nama,
        'alamat'=>$alamat,
        'jurusan'=>$jurusan,
        'email'=>$email
    ]);
    return redirect('/');
    }
}
